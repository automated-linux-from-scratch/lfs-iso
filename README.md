# Overview

Build the LFS ISO. This is the fifth and last step in the LFS pipeline.

Refer to the `docs` project for a general overview of the whole `automated-linux-from-scratch` group.

![LFS ISO demo](img/demo.gif)

# Build

Local build follows the `.gitlab-ci.yml`, but it has to be adjusted for building without `dind`. The idea is to
populate project's `build` directory with necessary artifacts and mount them into a `base-iso` container for processing.

Start by exporting `lfs-system` image into the `build/system` directory:
```shell script
$ mkdir -p build/system
$ docker run -itd --rm --name lfs-container lfs-system:dev
$ docker export -o build/lfs.tar lfs-container
$ docker stop lfs-container
$ tar xf build/lfs.tar -C build/system
```

Then, create a docker container from `base-iso` image with bind mounting project directory:
```shell script
$ docker run -it --rm -v $(pwd):/repo base-iso:dev
```

Execute the following operations within the docker container. ISO build uses the squashed LFS system and initramfs,
so it has to be executed last.

## Squashed LFS system

Squash the LFS system using, e.g. `4 CPUs`:

```shell script
# export CPUS=4
# mksquashfs /repo/build/system /repo/build/lfs.squashfs -comp xz -noappend -all-root -processors ${CPUS}
```

## Initramfs

Build initramfs:

```shell script
# mkdir -p /repo/build/initramfs/bin
# cp /usr/bin/busybox /repo/build/initramfs/bin/
# cp /repo/config/initramfs-init.sh /repo/build/initramfs/init
# cd /repo/build/initramfs
# find . -print0 | cpio --null --create --verbose --format=newc | gzip --best > ../initramfs.cpio.gz
```

## ISO

Build ISO:

```shell script
# mkdir -p /repo/build/live-cd/boot/grub
# cp /repo/config/grub.cfg /repo/build/live-cd/boot/grub/
# mv /repo/build/system/boot/vmlinuz-5.5.3-lfs-9.1-systemd /repo/build/live-cd/boot/
# mv /repo/build/lfs.squashfs /repo/build/live-cd/
# mv /repo/build/initramfs.cpio.gz /repo/build/live-cd/boot
# grub-mkrescue -o /repo/build/lfs-live-cd.iso /repo/build/live-cd
```

Exit the container. Final result `lfs-live-cd.iso` is available in your `build` directory.

# Run

In case you only want to try out the live-cd, head to this project's releases and download the image from there.

The final `lfs-live-cd.iso` image can be executed on, e.g. `qemu`:

```shell script
$ qemu-system-x86_64 -cdrom lfs-live-cd.iso -boot d -nographic
```

Default root user password is defined in `Dockerfile` under `ROOT_PASSWORD` in the `lfs-system` project.

Device with the root filesystem is mounted as read-only. On top of that, overlay filesystem is mounted in memory. This
means that no changes are preserved after shutting down the VM. See `config/initramfs-init.sh` script on how it's done.

Default boot config is available at `config/grub.cfg`. Kernel parameter `root` has to be supplied. `console`
parameter is set to `ttyS0` to ensure `-nographic` mode is working well in `qemu`.

You can also copy the image onto a pendrive and boot it as a live-usb system. Edit the `root` kernel parameter during 
boot, so that it points to your device and remove the `console` parameter. For example, assuming the device is at 
`/dev/sda`, you would modify the parameters in the GRUB boot menu to look like this:
```shell script
menuentry "GNU/Linux, Linux 5.5.3-lfs-9.1-systemd" {
    linux   /boot/vmlinuz-5.5.3-lfs-9.1-systemd root=/dev/sda
    initrd  /boot/initramfs.cpio.gz
}
```

In case you are dropped to an emergency shell, you can inspect where the `init` script failed and continue from there.

```shell script
# cat init
```
