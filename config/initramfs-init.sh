#!/bin/busybox sh

/bin/busybox mkdir -p /usr/sbin /usr/bin /sbin /bin
/bin/busybox --install -s

rescue_shell() {
    echo "Something went wrong. Dropping to a shell."
    exec sh
}

# Parse kernel parameters
cmdline() {
    local value
    value=" $(cat /proc/cmdline) "
    value="${value##* ${1}=}"
    value="${value%% *}"
    [ "${value}" != "" ] && echo "${value}"
}

# Mount the /proc and /sys filesystems
mkdir /proc /sys
mount -t proc none /proc
mount -t sysfs none /sys

# Disable kernel messages from popping onto the screen
echo 0 > /proc/sys/kernel/printk

# Clear the screen
clear
echo "Init in progress ..."

# Mount rootfs dir in memory
mkdir -pm 755 /run/rootfs
mount -t tmpfs -o size=100%,mode=755,suid,exec tmpfs /run/rootfs

# Create all dirs
mkdir -pm 755 /run/rootfs/drive /run/rootfs/ro /run/rootfs/rw /run/rootfs/.workdir /root

# Mount device with lfs system
# Requires root parameter to be passed
mount -t devtmpfs none /dev
echo "Waiting for the device $(cmdline root) to be ready ..."
sleep 5
echo "Mounting device $(cmdline root) ..."
mount -r $(findfs $(cmdline root)) /run/rootfs/drive || rescue_shell

echo "Mounting lfs.squashfs ..."
mount -t squashfs -o defaults,ro /run/rootfs/drive/lfs.squashfs /run/rootfs/ro || rescue_shell

echo "Mounting overlay ..."
mount -t overlay -o lowerdir=/run/rootfs/ro,upperdir=/run/rootfs/rw,workdir=/run/rootfs/.workdir root root/ || rescue_shell

echo "Unmounting proc, sys and dev ..."
umount proc
umount sys
umount dev

echo "Switching root ..."
exec switch_root /root /sbin/init || rescue_shell
